from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/tasks.readonly']


def main():
    """Shows basic usage of the Tasks API.
    Prints the title and ID of the first 10 task lists.
    """
    creds = setup()
    service = build('tasks', 'v1', credentials=creds)

    # Call the Tasks API
    results = service.tasklists().list(maxResults=50).execute()
    listOfLists = results.get('items', [])

    # ncmList = service.tasklists().get(tasklist="MDYwMDQwMjQyMzM4MTQyMzUwMzc6MTkwOTkxNTMzMzcwNzkyNDow").execute()
    ncmTasks = service.tasks().list(tasklist="MDYwMDQwMjQyMzM4MTQyMzUwMzc6MTkwOTkxNTMzMzcwNzkyNDow").execute()['items']
    professionalTasks = service.tasks().list(tasklist="MDYwMDQwMjQyMzM4MTQyMzUwMzc6MjM2NTgwNjQ0MzAyNzI2Njow").execute()['items']
    personalTasks = service.tasks().list(tasklist="MDYwMDQwMjQyMzM4MTQyMzUwMzc6MjY3NjAxNjE2MjU2MzkzMTow").execute()['items']
    schoolTasks = service.tasks().list(tasklist="MDYwMDQwMjQyMzM4MTQyMzUwMzc6MTAzOTgzNTAxMzM3NDQwNTow").execute()['items']

    prettyPrint('NCM', ncmTasks)
    prettyPrint('Professional', professionalTasks)
    prettyPrint('Personal', personalTasks)
    prettyPrint('School', schoolTasks)

    # print('NCM')
    # for task in ncmTasks:
    #     print('\t', task['title'])


    # if not listOfLists:
    #     print('No task lists found.')
    # else:
    #     print('Task lists:')
    #     for taskList in listOfLists:
    #         print(u'{0} ({1})'.format(taskList['title'], taskList['id']))


def setup():
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server()
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)
    return creds


def prettyPrint(taskListName, taskList):
    print(taskListName)
    for task in taskList:
        if 'due' in task:
            dueDate = task['due']
        else:
            dueDate = 'eventually'
        status = 'completed' in task
        print('\t', task['title'])
        print('\t\tDUE: ', dueDate)
        print('\t\tDONE: ', status)


if __name__ == '__main__':
    main()
